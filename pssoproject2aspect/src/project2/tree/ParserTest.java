
package project2.tree;

class ParserTest {
	public static void main(String[] args) {
		Parser parser = new Parser("1 + 2 * 3"); // spaces are vital!!
		TreeNode node = parser.parse();

		InFixPrintVisitor mv = new InFixPrintVisitor();
		PreFixPrintVisitor preV = new PreFixPrintVisitor();
		PostFixPrintVisitor postV = new PostFixPrintVisitor();
		EvaluateVisitor eV = new EvaluateVisitor();
		node.accept(mv);
		System.out.println();
		node.accept(preV);
		System.out.println();
		node.accept(postV);
		System.out.println();
		node.accept(eV);
		System.out.println("result: " + eV.getValue());

	}

}
