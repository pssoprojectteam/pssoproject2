package project2.tree;

import project2.visitor.VisitorProtocol.Leaf;
import project2.visitor.VisitorProtocol.Node;
import project2.visitor.VisitorProtocol.Visitable;
import project2.visitor.VisitorProtocol.Visitor;

public class PostFixPrintVisitor implements Visitor {

	public void visit(Node node) {

		Visitable[] elements = node.getAll();
		Visitable left = elements[0];
		Visitable right = elements[1];

		if (left != null)
			left.accept(this);

		if (right != null)
			right.accept(this);

		System.out.print(node.getLabelProt());

	}

	public void visit(Leaf leaf) {
		System.out.print(leaf.getLabelProt());
	}

	public PostFixPrintVisitor() {

	}

}
