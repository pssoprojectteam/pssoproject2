package project2.tree;

import java.util.Stack;

import project2.visitor.VisitorProtocol.Leaf;
import project2.visitor.VisitorProtocol.Node;
import project2.visitor.VisitorProtocol.Visitable;
import project2.visitor.VisitorProtocol.Visitor;

public class EvaluateVisitor implements Visitor {
	private Stack<Integer> stack = new Stack<>();

	public int getValue() {
		return stack.peek();
	}

	public void visit(Node node) {
		Visitable[] elements = node.getAll();
		Visitable left = elements[0];
		Visitable right = elements[1];

		if (left != null)
			left.accept(this);
		if (right != null)
			right.accept(this);
		int rightValue = stack.pop();
		int leftValue = stack.pop();
		stack.push(node.computeProt(leftValue, rightValue));

	}

	public void visit(Leaf leaf) {
		stack.push(leaf.getValueProt());
	}

	public EvaluateVisitor() {

	}
}
