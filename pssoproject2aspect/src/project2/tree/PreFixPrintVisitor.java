package project2.tree;

import project2.visitor.VisitorProtocol.Leaf;
import project2.visitor.VisitorProtocol.Node;
import project2.visitor.VisitorProtocol.Visitable;
import project2.visitor.VisitorProtocol.Visitor;

public class PreFixPrintVisitor implements Visitor {

	public void visit(Node node) {

		Visitable[] elements = node.getAll();
		Visitable left = elements[0];
		Visitable right = elements[1];
		System.out.print(node.getLabelProt());

		if (left != null)
			left.accept(this);

		if (right != null)
			right.accept(this);

	}

	public void visit(Leaf leaf) {
		System.out.print(leaf.getLabelProt());
	}

	public PreFixPrintVisitor() {

	}

}
