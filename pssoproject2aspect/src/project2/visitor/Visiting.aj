package project2.visitor;

import project2.tree.*;
import java.lang.String;;

public aspect Visiting extends VisitorProtocol {
	declare parents : TreeNode implements Visitable;
	declare parents : BinaryOperatorNode implements Node;
	declare parents : NumericNode implements Leaf;

	public Visitable[] BinaryOperatorNode.getAll() {
		Visitable[] elements = new Visitable[2];
		elements[0] = this.getLeft();
		elements[1] = this.getRight();
		return elements;
	}

	public int BinaryOperatorNode.computeProt(int a, int b) {
		return this.compute(a, b);
	}

	public String TreeNode.getLabelProt() {
		return this.getLabel();
	}

	public int NumericNode.getValueProt() {
		return this.getValue();
	}

}
