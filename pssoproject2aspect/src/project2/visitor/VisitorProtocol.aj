package project2.visitor;

public abstract aspect VisitorProtocol {
	public interface Visitable {
		public void accept(Visitor visitor);
		public String getLabelProt();
	}

	public interface Leaf extends Visitable {
		public int getValueProt();
	}

	public interface Node extends Visitable {
		public Visitable[] getAll();
		public int computeProt(int a, int b);
	}

	public interface Visitor {
		public void visit(Node node);
		public void visit(Leaf leaf);
	}
	
	public void Visitable.accept() {
		
	}
	
	public void Node.accept(Visitor visitor) {
		visitor.visit(this);
	}

	public void Leaf.accept(Visitor visitor) {
		visitor.visit(this);
	}
}
